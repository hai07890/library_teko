"""django_restful URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from webapplication import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.homepage),
    path('login', views.login_page),
    path('logout', views.view_logout),
    path('user', views.view_user, name='user-panel'),
    path('update_user_info', views.update_user_info),
    path('book/', views.view_book),
    path('author/', views.viewAuthor),
    #path('view_borrow/', views.view_borrow),
    path('borrow_book', views.borrow_book),
    path('add_book_copy/', views.addBook),
    path('view_borrow_random/', views.view_borrow_random),
    path('search_book', views.view_search_book),
    path('search_author', views.view_search_author),
    path('login_with_google/', views.login_with_google),
    path('oauth2callback', views.oauth2callback),
]