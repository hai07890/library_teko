from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db import connection
from .models import User, Author, Borrow, Book, ReturnBook, BookCopy
from .form import *
from django.contrib.auth.models import User as AuthUser
from django.contrib.auth import authenticate, login, logout
from datetime import datetime, timedelta
from django.db.utils import OperationalError, DatabaseError, NotSupportedError, IntegrityError
from django.db.models import Q
from oauth2client.client import OAuth2WebServerFlow
import google.oauth2.credentials
import google_auth_oauthlib.flow
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
import random
import time
import pytz
import json
import logging
import urllib.request
import secrets
import string
from django.http import Http404
# Create your views here.
###user


def homepage(request):
    return render(request, 'homepage.html', {
        'isLoggedIn': my_authenticate(request),
        'username': request.session.get('username', None)
    }, content_type='text/html')


def login_page(request):
    if my_authenticate(request):
        return redirect(homepage)
    else:
        return render(request, 'login.html', {

        }, content_type="text/html")


# done
def my_authenticate(request):
    if request.user.is_authenticated:
        return True

    if request.method == "POST":
        form = LoginForm(request.POST)
    elif request.method == "GET":
        form = LoginForm(request.GET)

    if form.is_valid():
        try:
            user = authenticate(username=form.username, password=form.password)
            if user is not None:
                login(request, user)
                request.session['username'] = form.username
                if form.rememberme:
                    request.session.set_expiry(60 * 60 * 24 * 30) #1 month
                return True
            else:
                return False
        except AuthUser.DoesNotExist:
            return False

    return False


def login_with_google(request):
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        client_secrets_file='client_secret.json',
        scopes=['profile', 'email'])
    flow.redirect_uri = 'http://localhost:8000/oauth2callback'
    authorization_url, state = flow.authorization_url(
        # Enable offline access so that you can refresh an access token without
        # re-prompting the user for permission. Recommended for web server apps.
        access_type='offline',
        # Enable incremental authorization. Recommended as a best practice.
        include_granted_scopes='true')

    return redirect(authorization_url)


def oauth2callback(request):
    state = request.GET.get('state')
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        'client_secret.json',
        scopes=['profile', 'email'],
        state=state)
    flow.redirect_uri = request.build_absolute_uri(request.path)

    authorization_response = request.build_absolute_uri(request.get_full_path())
    flow.fetch_token(code=request.GET.get('code'))

    # Store the credentials in the session.
    # ACTION ITEM for developers:
    #     Store user's access and refresh tokens in your data store if
    #     incorporating this code into your real app.
    credentials = flow.credentials
    request.session['credentials'] = {
        'token': credentials.token,
        'refresh_token': credentials.refresh_token,
        'token_uri': credentials.token_uri,
        'client_id': credentials.client_id,
        'client_secret': credentials.client_secret,
        'scopes': credentials.scopes}

    authenticate_google_user(request, credentials.token)

    return redirect("/")


def authenticate_google_user(request, access_token):
    content = urllib.request.urlopen("https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + access_token)
    user_info = json.load(content)
    if user_info.get('error'):
        return False

    try:
        user = AuthUser.objects.get(email=user_info.get('email'))
        login(request, user)
        request.session['username'] = user.username
    except ObjectDoesNotExist:
        password = ''.join(secrets.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(128))
        user = AuthUser.objects.create_user(user_info.get('id'), user_info.get('email'), password)
        user.first_name = user_info.get('given_name')
        user.last_name = user_info.get('family_name')
        user.save()
        user_addition_info = User.objects.create(address="", phone_number="", auth_user_id=user.id)
        user_addition_info.save()

        login(request, user)
        request.session['username'] = user.username


@login_required
def view_user(request):
    username = request.session.get('username')
    user = AuthUser.objects.get(username=username)
    user_info = User.objects.get(auth_user_id=user.id)
    return render(request, 'user.html', {
        'username': request.session.get('username', None),
        'isLoggedIn': my_authenticate(request),
        'first_name': user.first_name,
        'last_name': user.last_name,
        'email': user.email,
        'phone_number': user_info.phone_number,
        'address': user_info.address,
    }, content_type='text/html')


@login_required
def update_user_info(request):
    if request.method == 'POST':
        form = UpdateUserInfoForm(request.POST)
    elif request.method == 'GET':
        form = UpdateUserInfoForm(request.GET)

    username = request.session.get('username')
    if form.is_valid():
        user = AuthUser.objects.get(username=username)
        user.username = form.cleaned_data['username']
        user.first_name = form.cleaned_data['first_name']
        user.last_name = form.cleaned_data['last_name']
        user_info = User.objects.get(auth_user_id=user.id)
        user_info.phone_number = form.cleaned_data['phone_number']
        user_info.address = form.cleaned_data['address']

        with transaction.atomic():
            user.save()
            user_info.save()

        request.session['username'] = user.username
        login(request, user)

    return redirect('/user')


def view_logout(request):
    logout(request)
    if request.session.get('username', None):
        del request.session['username']
    request.session.set_expiry(0)
    return redirect(login_page)


def view_book(request):
    books = Book.objects.all()
    return render(request, 'viewbook.html', {
        'isLoggedIn': my_authenticate(request),
        'username': request.session.get('username', None),
        'books': books
    }, content_type='text/html')


def view_search_book(request):
    if request.method == 'POST':
        if request.POST.get('search', None):
            return search_book(request)

    return render(request, 'viewsearchbook.html', {
        'isLoggedIn': my_authenticate(request),
        'username': request.session.get('username', None),
    }, content_type='text/html')


def search_book(request):
    if request.method == 'POST':
        form = SearchBookForm(request.POST)
    elif request.method == 'GET':
        form = SearchBookForm(request.GET)

    books = Book.objects.all()
    if form.is_valid():
        if form.cleaned_data['book_name']:
            books = books.filter(Q(name__icontains=form.cleaned_data['book_name']))
        elif form.cleaned_data['author_name']:
            books = books.filter(Q(authors__name__icontains=form.cleaned_data['author_name']))
        elif form.cleaned_data['publish_company']:
            books = books.filter(Q(publish_company__icontains=form.cleaned_data['publish_company']))
        elif form.cleaned_data['publish_from'] and form.cleaned_data['publish_to']:
            books = books.filter(Q(publish_date__range=(form.cleaned_data['publish_from'], form.cleaned_data['publish_to'])))

        return render(request, 'viewbook.html', {
            'isLoggedIn': my_authenticate(request),
            'username': request.session.get('username', None),
            'books': books,
        }, content_type='text/html')
    else:
        return HttpResponse(form.errors)


def view_search_author(request):
    if request.method == 'POST':
        return search_author(request)

    form = SearchAuthorForm()
    return render(request, 'viewsearchauthor.html', {
        'isLoggedIn': my_authenticate(request),
        'username': request.session.get('username', None),
        'form': form
    }, content_type='text/html')


def search_author(request):
    if request.method == 'POST':
        form = SearchAuthorForm(request.POST)
    elif request.method == 'GET':
        form = SearchAuthorForm(request.GET)

    authors = Author.objects.all()
    if form.is_valid():
        if form.cleaned_data['name']:
            authors = authors.filter(Q(name__icontains=form.cleaned_data['name']))
        elif form.cleaned_data['pen_name']:
            authors = authors.filter(Q(pen_name__icontains=form.cleaned_data['pen_name']))
        elif form.cleaned_data['address']:
            authors = authors.filter(Q(address__icontains=form.cleaned_data['address']))
        elif form.cleaned_data['phone_number']:
            authors = authors.filter(Q(phone_number=form.cleaned_data['phone_number']))

        return render(request, 'viewauthor.html', {
            'isLoggedIn': my_authenticate(request),
            'username': request.session.get('username', None),
            'authors': authors
        }, content_type='text/html')


def viewAuthor(request):
    authors = Author.objects.all()
    return render(request, 'viewauthor.html', {
        'isLoggedIn': my_authenticate(request),
        'username': request.session.get('username', None),
        'authors': authors
    }, content_type='text/html')


def view_borrow_random(request):
    if my_authenticate(request):
        if request.method == 'POST':
            book_id = request.POST.get('book_id', None)

        if book_id is not None:
            book = Book.objects.get(id=book_id)
            username = request.session.get('username', None)
            user = AuthUser.objects.get(username=username)
            bookcopys = BookCopy.objects.filter(book_id=book_id, can_borrow=True)
            num_available = len(bookcopys.all())
            bookcopy_index = random.randint(0, num_available - 1)

            vn_timezone = pytz.timezone("Asia/Ho_Chi_Minh")

            return render(request, 'view_borrow_random.html', {
                'isLoggedIn': my_authenticate(request),
                'username': request.session.get('username', None),
                'bookcopy': bookcopys[bookcopy_index],
                'book': book,
                'info': ', '.join(filter(lambda s: s != "", [bookcopys[bookcopy_index].quality, ReturnBook.getQualityInfo(bookcopy_index)])),
                'current_time': datetime.now(vn_timezone).strftime('%d/%m/%Y %H:%M:%S'),
                'day_time': datetime.now(vn_timezone).strftime('%H:%M:%S'),
                'current_date': datetime.now(vn_timezone).strftime('%d-%m-%Y'),
            }, content_type='text/html')


def borrow_book(request):
    if my_authenticate(request):
        if request.method == 'POST':
            form = BorrowBookForm(request.POST)
        elif request.method == 'GET':
            form = BorrowBookForm(request.GET)

        username = request.session.get('username', None)
        user = AuthUser.objects.get(username=username)
        user_info = User.objects.get(auth_user_id=user.id)

        if form.is_valid():
            try:
                with connection.cursor() as cursor:
                    cursor.execute("call borrow(%s, %s, %s);", [form.cleaned_data['bookcopy_id'], form.cleaned_data['end_date'], user_info.id])
                return HttpResponse('Borrow Successful')
            except (OperationalError, DatabaseError, NotSupportedError, IntegrityError) as e:
                return HttpResponse(e.__cause__)

    return HttpResponse('Borrow Failed')


# def view_borrow(request):
#     id = request.POST.get("id", None)
#     id = request.GET.get("id", None)
#     id = int(id)
#     bookcopys = BookCopy.get_available_book(id)
#     book = Book.objects.get(id=id)
#     bookcopy_info = {}
#     for bookcopy in bookcopys:
#         bookcopy_info[bookcopy.id] = ReturnBook.getQualityInfo(bookcopy.id)
#     return render(request, 'borrow.html', {
#         'isLoggedIn': my_authenticate(request),
#         'username': request.session.get('username', None),
#         'bookcopys': bookcopys,
#         'book': book,
#         'info': bookcopy_info
#     }, content_type='text/html')

#
# def viewBorrowed(request):
#     user = authenticate(username='hai07890', password='123456')
#     if user is not None:
#         books = Book.objects.get(name="")
#         res = 'book: '
#         for book in books:
#             res += 'name %s <br/>' % book.name
#         return HttpResponse(res)
#     else:
#         return redirect(homepage)



### admin
def addBook(request):

    BookCopy.bulk_create(1, 100, "good", 50)


def receiveBook(request):
    pass