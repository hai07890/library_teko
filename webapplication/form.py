from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(label='username', max_length=150)
    password = forms.CharField(label='password', max_length=128)
    rememberme = forms.BooleanField(label='remember-me')


class SignupForm(forms.Form):
    username = forms.CharField(label='Username', max_length=150)
    password = forms.CharField(label='Password', max_length=128, type='password')
    first_name = forms.CharField(label='First Name', max_length=30)
    last_name = forms.CharField(label='Last Name', max_length=150)
    address = forms.CharField(label='Address', max_length=500)
    phone_number = forms.CharField(label='')


class BorrowBookForm(forms.Form):
    bookcopy_id = forms.IntegerField(label='bookcopy_id')
    end_date = forms.DateTimeField(label='end_date')


class SearchBookForm(forms.Form):
    book_name = forms.CharField(label='Book Name', max_length=50, required=False)
    author_name = forms.CharField(label='Author Name', max_length=50, required=False)
    publish_company = forms.CharField(label='Publish Company', max_length=50, required=False)
    publish_from = forms.DateTimeField(label='Publish From', required=False)
    publish_to = forms.DateTimeField(label='Publish To', required=False)


class SearchAuthorForm(forms.Form):
    name = forms.CharField(label='Author name', max_length=50, required=False)
    pen_name = forms.CharField(label='Pen name', max_length=50, required=False)
    address = forms.CharField(label='Address', max_length=50, required=False)
    phone_number = forms.CharField(label='Phone Number', max_length=50, required=False)


class UpdateUserInfoForm(forms.Form):
    username = forms.CharField(label='username', max_length=150)
    first_name = forms.CharField(label='first_name', max_length=30, required=False)
    last_name = forms.CharField(label='last_name', max_length=150, required=False)
    phone_number = forms.CharField(label='phone_number', max_length=30, required=False)
    address = forms.CharField(label='address', max_length=500, required=False)


