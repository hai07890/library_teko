from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(User)
admin.site.register(Author)
admin.site.register(Borrow)
admin.site.register(Book)
admin.site.register(ReturnBook)
admin.site.register(BookCopy)
admin.site.register(Genre)



# class AddBookAdmin(admin.ModelAdmin):
#     fieldsets = (
#         (None, {
#             'fields': ('book name', 'corver img', 'publish_date', 'publish_company', 'short description', 'long description', )
#         })
#     )