# Generated by Django 2.0.6 on 2018-06-26 07:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapplication', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='long_description',
            field=models.CharField(max_length=2000, null=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='preview_link',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
