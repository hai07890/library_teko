# Generated by Django 2.0.6 on 2018-06-26 07:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapplication', '0002_auto_20180626_0710'),
    ]

    operations = [
        migrations.RenameField(
            model_name='borrow',
            old_name='time_period',
            new_name='expiry_date',
        ),
    ]
