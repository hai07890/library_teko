from django.db import models
from django.contrib.auth.models import User as AuthUser
from datetime import datetime
from django.utils import timezone
from django.db import connection
import random

# Create your models here.


class User(models.Model):
    auth_user = models.OneToOneField(AuthUser, on_delete=models.CASCADE, default=None)
    address = models.CharField(max_length = 500)
    phone_number = models.CharField(max_length = 30)

    class Meta:
        db_table = "user"


class Book(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length = 50)
    cover_img = models.CharField(max_length=300)
    publish_date = models.DateTimeField()
    publish_company = models.CharField(max_length = 50)
    short_description = models.CharField(max_length=300)
    long_description = models.CharField(max_length=2000, null=True)
    preview_link = models.CharField(max_length=200, null=True)
    
    class Meta:
        db_table = "book"

    @staticmethod
    def getAuthorsPenName(book):
        return list(map(lambda author: author.pen_name, book.authors.all()))

class Author(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length = 50, null=True)
    pen_name = models.CharField(max_length = 50, unique=True)
    address = models.CharField(max_length = 50, null=True)
    phone_number = models.CharField(max_length = 30, null=True)
    email = models.CharField(max_length = 50, null=True)
    books = models.ManyToManyField(Book, related_name="authors")
    
    class Meta:
        db_table = "author"


class BookCopy(models.Model):
    id = models.AutoField(primary_key=True)
    quality = models.CharField(max_length = 50, null=True)
    can_borrow = models.BooleanField(default=False)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    
    class Meta:
        db_table = "bookcopy"

    def get_available_book(book_id):
        bookcopys = BookCopy.objects.raw("select * from book\
                                            join bookcopy on bookcopy.book_id = book.id\
                                            left join borrow on borrow.bookcopy_id = bookcopy.id\
                                            left join returnbook on returnbook.borrow_id = borrow.id\
                                            where can_borrow = 1 and (time is not NULL or borrow.id is NULL) and book.id = %d;" % book_id)
        size = len(list(bookcopys))
        if size == 0:
            return None
        else:
            return bookcopys

    @staticmethod
    def bulk_create(book_id, num_book, quality, can_borrow_num):
        book = Book.objects.get(id=book_id)
        bookcopy_can_borrow = []
        for i in range(can_borrow_num):
            bookcopy_can_borrow.append(BookCopy(quality=quality, can_borrow=1, book=book))
        bookcopy_no_borrow = []
        for i in range(num_book - can_borrow_num):
            bookcopy_no_borrow.append(BookCopy(quality=quality, can_borrow=0, book=book))
        BookCopy.objects.bulk_create(bookcopy_no_borrow, 50)
        BookCopy.objects.bulk_create(bookcopy_can_borrow, 50)


class Borrow(models.Model):
    id = models.AutoField(primary_key=True)
    borrow_date = models.DateTimeField(default=datetime.now)
    expiry_date = models.DateTimeField()
    bookcopy = models.ForeignKey(BookCopy, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    
    class Meta:
        db_table = "borrow"

    @staticmethod
    def borrow(bookcopy_id, expiry_date, user_id):
        with connection.cursor() as cursor:
            cursor.execute("CALL borrow(%d, %s, %d)", [bookcopy_id, expiry_date, user_id])

class ReturnBook(models.Model):
    borrow = models.OneToOneField(Borrow, primary_key=True, on_delete=models.CASCADE)
    changes_quality = models.CharField(max_length = 500, null=True)
    penalty = models.CharField(max_length = 500, null=True)
    time = models.DateTimeField(default=timezone.now)
    
    class Meta:
        db_table = "returnbook"

    def getQualityInfo(bookcopy_id):
        returnbook = ReturnBook.objects.select_related('borrow__bookcopy').filter(borrow__bookcopy__book_id = bookcopy_id)
        totalQuality = ', '.join(map(lambda quality: quality.changes_quality, list(returnbook)))
        return totalQuality


class Genre(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=300)
    models.ManyToManyField(Book, related_name="genres")

    class Meta:
        db_table = "genre"